<?php

namespace App\Console\Commands;

use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Log;

class DownloadDoubts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:download-doubts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para realizar requisições em plataforma de atendimento';

    private Http $client;
    private CookieJar $jar;
    private array $headers;

    public function __construct()
    {
        parent::__construct();

        $this->client = new Http();
        $this->jar = new CookieJar();
        $this->headers = [
            'Authority' => 'www.clubealuno.com.br',
            'Accept' => '*/*',
            'Sec-Fetch-Dest' => 'empty',
            'Sec-Fetch-Mode' => 'cors',
            'Sec-Fetch-Site' => 'same-origin',
            'Sec-Gpc' => '1',
            'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
            'X-Requested-With' => 'XMLHttpRequest'
        ];
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        Log::info("Comando acionado");
        $this->info('Iniciando comando');

        $credentials = [
            env("AUTH_TOKEN_1")
        ];

        foreach ($credentials as $credential){
            for($i = 0; $i <= 500; $i++){
                $response = $this->dataRequest();
                $this->info("Interação Nº $i");
                if(strlen($response->body()) > 20) {
                    $this->loginRequest($credential);
                }

                ($i % 20 == 0) ? sleep(5) : false;
            }

            $this->info('Comando Finalizado em '.now()->toDateTimeString());
            Log::info("Comando finalizado {Ultima response: }".$response->body());
        }

        return true;
    }

    public function loginRequest($loginDetails)
    {
        $this->info('Usuário não está logado');
        $this->info("Fazendo requisição de Login");

        $this->client::withOptions(['cookies' => $this->jar])
            ->get('https://www.clubealuno.com.br/painel/login');

        $this->client::withOptions(['cookies' => $this->jar])
            ->withHeaders($this->headers)
            ->withBody($loginDetails, 'application/x-www-form-urlencoded; charset=UTF-8')
            ->post('https://www.clubealuno.com.br/painel/login/validate_login?return=undefined');

        $this->info("Requisição de Login Finalizada");
    }

    public function dataRequest(): \GuzzleHttp\Promise\PromiseInterface|\Illuminate\Http\Client\Response
    {
        $this->info("Fazendo requisição para baixar informações");

        return $this->client::withOptions(['cookies' => $this->jar])
            ->withHeaders($this->headers)
            ->get('https://www.clubealuno.com.br/painel/pedagogico/duvidas/baixarduvidas');
    }
}
