<?php

namespace App\Http\Controllers;

use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Support\Facades\Http;

class RequestController extends Controller
{
    private Http $client;
    private CookieJar $jar;
    private array $headers;

    public function __construct()
    {
        $this->client = new Http();
        $this->jar = new CookieJar();
        $this->headers = [
            'Authority' => 'www.clubealuno.com.br',
            'Accept' => '*/*',
            'Sec-Fetch-Dest' => 'empty',
            'Sec-Fetch-Mode' => 'cors',
            'Sec-Fetch-Site' => 'same-origin',
            'Sec-Gpc' => '1',
            'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
            'X-Requested-With' => 'XMLHttpRequest'
        ];
    }

    public function index()
    {
        for($i = 0; $i <= 2; $i++){
            $response = $this->dataRequest();
            if(strlen($response->body()) > 20) {
                $this->loginRequest();
            }
        }

        dd($response, $i, $response->body());
    }


    public function loginRequest()
    {
        $loginDetails = "perfil=2&login=tatianepereirafernandes10%40gmail.com&senha=6GA(kjrE";

        $this->client::withOptions(['cookies' => $this->jar])
            ->get('https://www.clubealuno.com.br/painel/login');

        $this->client::withOptions(['cookies' => $this->jar])
            ->withHeaders($this->headers)
            ->withBody($loginDetails, 'application/x-www-form-urlencoded; charset=UTF-8')
            ->post('https://www.clubealuno.com.br/painel/login/validate_login?return=undefined');

    }

    public function dataRequest(): \GuzzleHttp\Promise\PromiseInterface|\Illuminate\Http\Client\Response
    {
        return $this->client::withOptions(['cookies' => $this->jar])
            ->withHeaders($this->headers)
            ->get('https://www.clubealuno.com.br/painel/pedagogico/duvidas/baixarduvidas');
    }

}
